![REDPOL_LOGO](https://git.unicaen.fr/nicolas.elie/redpol-open/-/raw/master/media/REDPOL_LOGO.jpg)

## _Procedure to analyze our raw images sequences were taken with a colour camera mounted on a stereomicroscope._

Anaïd Gouveneaux (1,2), Mamoudou Sano (3), Nicolas Elie (3), Apolline Chabenat (1,2), Céline Thomasse (2), Christelle Jozet-Alves (2), Anne-Sophie Darmaillacq (2), Ludovic Dickel(2), Thomas Knigge (1), Cécile Bellanger (2),

* 1 Normandie Univ, UNILEHAVRE, UMR-I02, Environmental stress and biomonitoring of aquatic environments (SEBIO), 76600 Le Havre, France
* 2 Normandie Univ, UNICAEN, UMR 6552, Rennes 1-CNRS (EthoS) - Cephalopod cognitive neuroethology (NECC), 14000 Caen, France
* 3 Normandie Univ, UNICAEN, CMAbio3 - Centre de Microscopie Appliquée à la biologie

# Presentation

The purpose of this page is to present the analysis approach that has been set up for the analysis of chromatopores on video sequences.

# Summary presentation of the process :

The presentation of the project can be found on [the wiki page](https://git.unicaen.fr/nicolas.elie/redpol-open/-/wikis/home).

# Installation

The procedure has been tested with linux : Ubuntu 20.04 LTS/22.04.3 LTS

The project is mainly based on the Stardist method: https://github.com/stardist/stardist/.

The proposed installation uses Conda environment: https://docs.conda.io/en/latest/.


1. Require java on your system
Install the OpenJDK 11 
```
sudo apt-get install openjdk-11-jdk
```

2. CONDA: Installation on Linux
https://conda.io/projects/conda/en/latest/user-guide/install/linux.html

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

chmod +x Miniconda3-latest-Linux-x86_64.sh

bash Miniconda3-latest-Linux-x86_64.sh -p $HOME/miniconda3
```

$HOME you can indicate your directory

Close your terminal  

3. Download project : 
```
git clone https://git.unicaen.fr/nicolas.elie/redpol-open.git
```
_Size project : (~ 2.5 Go)_

4.  Access to folder project
```
cd redpol-open/
```
5. You can install an conda environment via the provided environment file(s): requirements.yml.
```
conda env create -f requirements.yml
```
After installation of conda environment, you can launch redpol environment
```
conda activate redpol
```
6. Next, you use pip to install specific packages :
```
pip install numpy==1.23.1 
```
7. Then,
```
pip install -r requirements.txt
```


# Procedure

#### To test the execution of the AnalyzeSequence.ipynb program

The AnalyzeSequence.ipnyb program allows to use our Stardist model on each image of the vsi file.

The program process a detection and classification of chromatophores and save a file results.

In this project, We provide an image and our model to test our programs : 

- 'Examples/Process_13.vsi' 
-  Our model Stardist Model :  'models/stardist_multiclass'

To test programs without use command lines :

You can use [JupyterLab](https://jupyter.org/): A Notebook Interface.
JupyterLab is a web-based interactive development environment for notebooks

```
jupyter-lab
```
You can launch program by module and modify script with the notebook

Run _AnalyzeSequence.ipnyb_ program on conda environment
```
jupyter nbconvert --execute --clear-output AnalyzeSequence.ipynb
```
#### To build a new model

1. Annotation of the images with Qupath : https://qupath.github.io/

> Bankhead, P. et al. **QuPath: Open source software for digital pathology image analysis**. _Scientific Reports_ (2017). \
> https://doi.org/10.1038/s41598-017-17204-5


On your images, annotate the objects that interest you. If you have categories, remember to declare the different classes. For details on how to use Qupath, see the documentation on their site.

![QUPATH ILLUSTRATION](https://git.unicaen.fr/nicolas.elie/redpol-open/-/raw/master/media/qupath.jpg)

2. Once your annotations are done, use the script "_Export Annotations gson.groovy_" to export the annotations as a json file in Qupath.
3. Then use the python program "_JSON_Qupath_to_ImageLabel.ipynb_" which will allow to format the images and annotations so that they can be used by the program "_Model_Build.ipynb_." 
It is possible to test code with a small series with images examples :
-  _Examples/0001.tif_ and _Examples/0001b.tif_ 
-  JSON files on _Examples/qupathProject/ExportJSON_

```
jupyter nbconvert --execute --clear-output JSON_Qupath_to_ImageLabel.ipynb
```

4. Configure and execute _Model_Build.ipynb_.

This program is directly from an example provided on the github Stardist repository.

```
jupyter nbconvert --execute --clear-output Model_Build.ipynb
```

It is possible to test code with a small series with images examples : 0001.tif and 0001b.tif and JSON files

```
jupyter nbconvert --execute --clear-output Model_Build_short_example.ipynb
```

It is a simple test, the given model is not efficiency

> **_Be careful, for that Model_Build works, you need a compatible graphics card (CUDA)._**

5. The program "_AnalyzeSequence.ipnyb_" can then be used by designating your own model in the program code.
