selectAnnotations();
// Allows to remove holes in objects and small fragments as well as to simplify the contours.
// menu Objects>Annotations>Simplify Shape
runPlugin('qupath.lib.plugins.objects.RefineAnnotationsPlugin', '{"minFragmentSizeMicrons": 100.0,  "maxHoleSizeMicrons": 100.0}');
resetSelection();
boolean prettyPrint = false // false results in smaller file sizes and thus faster loading times, at the cost of nice formating
def gson = GsonTools.getInstance(prettyPrint)
def annotations = getAnnotationObjects()

def imageData = getCurrentImageData()
def name = GeneralTools.getNameWithoutExtension(imageData.getServer().getMetadata().getName())
// savec json files in folder ExportJSON on folder project Qupath.
def pathOutput = buildFilePath(PROJECT_BASE_DIR, 'ExportJSON')
mkdirs(pathOutput)

def outfname=pathOutput+File.separator +name+".json"


File file = new File(outfname)
file.withWriter('UTF-8') {
    gson.toJson(annotations,it)
}
